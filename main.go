package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strings"

	rbt "github.com/emirpasic/gods/trees/redblacktree"
)

type byLength []string

func (s byLength) Len() int {
	return len(s)
}
func (s byLength) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byLength) Less(i, j int) bool {
	return len(s[i]) > len(s[j])
}

func join(ins []rune, c rune) (result []string) {
	for i := 0; i <= len(ins); i++ {
		result = append(result, string(ins[:i])+string(c)+string(ins[i:]))
	}
	return
}

func permutations(testStr string) []string {
	var n func(testStr []rune, p []string) []string
	n = func(testStr []rune, p []string) []string {
		if len(testStr) == 0 {
			return p
		} else {
			result := []string{}
			for _, e := range p {
				result = append(result, join([]rune(e), testStr[0])...)
			}
			return n(testStr[1:], result)
		}
	}

	output := []rune(testStr)
	return n(output[1:], []string{string(output[0])})
}
func rotate(rotstr string) string {
	return rotstr[1:] + string(rotstr[0])
}
func all_possibilities(scrabble string) []string {
	if len(scrabble) != 7 {
		return nil
	}
	ret := permutations(scrabble)
	for i := 0; i < len(scrabble); i++ {
		ret = append(ret, permutations(scrabble[1:])...)
		ret = append(ret, permutations(scrabble[2:])...)
		ret = append(ret, permutations(scrabble[3:])...)
		ret = append(ret, permutations(scrabble[4:])...)
		ret = append(ret, permutations(scrabble[5:])...)
		scrabble = rotate(scrabble)
	}
	return ret
}

func main() {
	/*read file and put words in red-black tree*/
	b, err := ioutil.ReadFile("slowa.txt") // just pass the file name
	if err != nil {
		fmt.Print(err)
	}
	str := string(b)
	words := strings.Split(str, "\r\n")
	tree := rbt.NewWithStringComparator()
	for i := range words {
		tree.Put(words[i], nil)
	}
	/* test if all works great */
	_, is_found := tree.Get("butelka")
	if is_found {
		fmt.Println("juhu")
	} else {
		fmt.Println("nie juhu")
	}
	/*server side func*/
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		set := make(map[string]struct{})
		x := r.URL.Query().Get("str")
		d := all_possibilities(x)
		for i := 0; i < len(d); i++ {
			_, is_found = tree.Get(d[i])
			if is_found {
				//results = append(results, d[i])
				set[d[i]] = struct{}{}
			}
		}
		results := []string{}
		for key := range set {
			results = append(results, key)
		}
		sort.Sort(byLength(results))
		for i := 0; i < len(results); i++ {
			w.Write([]byte(results[i]))
			w.Write([]byte("\n"))
		}
	})
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
